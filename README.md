#Linky

Linky backend with **HapiJS** written with *Coffeescript*

Install dependencies

```
npm install
npm start 
```

##TODO
- Endpoints
  - /api/users/
  - /api/users/create/
  - /api/users/{user-id}/followers
  - /api/users/{user-id}/followings
  - ...
- Documentation
- Design Models
  - User
  - Link
  - Admin
- DB Flow ( MongoDB *(mongoose)* )
- Tokenization
- Google Account
