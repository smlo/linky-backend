"use strict"

gulp = require "gulp"
coffee = require 'gulp-coffee'


www =
    coffee : ['source/*.coffee']
    js : ['dist']

gulp.task 'coffee', ->
    gulp.src(www.coffee)
    .pipe(coffee())
    .pipe(gulp.dest(www.js + '/js'))

gulp.task 'default', ->
    gulp.run(['coffee'])
