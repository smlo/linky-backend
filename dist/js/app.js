(function() {
  var User, UserSchema, _, hapi, mongoose, pretty, server;

  hapi = require('hapi');

  mongoose = require('mongoose');

  _ = require('underscore');

  pretty = require('prettyjson');

  server = new hapi.Server();

  mongoose.connect('mongodb://localhost/test');

  UserSchema = new mongoose.Schema({
    name: {
      type: String
    }
  });

  User = mongoose.model('User', UserSchema);

  server.connection({
    port: 3000
  });

  server.route({
    method: 'GET',
    path: '/api/users/{username}',
    handler: function(request, reply) {
      var user;
      user = new User({
        name: request.params.username
      });
      user.save();
      return reply('Hello, ' + encodeURIComponent(request.params.username) + '!');
    }
  });

  server.route({
    method: 'GET',
    path: '/api/users',
    handler: function(request, reply) {
      return User.find({}, function(err, documents) {
        return reply(documents);
      });
    }
  });

  server.start(function() {
    return console.log('Server running at ' + server.info.uri);
  });

}).call(this);
